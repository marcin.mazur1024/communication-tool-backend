# CommunicationTool

## Description
The Communication Tool REST API serves as the backend for a message exchange application, facilitating seamless communication between users. With endpoints for user registration, login, logout, and message creation, this API provides secure access to the application's features. Users can authenticate themselves, send and receive messages, and manage their sessions effortlessly. Built on RESTful principles, the Communication Tool API offers simplicity, reliability, and scalability for a smooth messaging experience.

## Author
Marcin Mazur

## How to run?
- download the project
- create database
- fill database details in the .env file
- run composer install
- run php artisan migrate to create database tables
- run php artisan db:seed to create a command device user

# START IN SEPARATE TERMINALS
- php artisan serve
- php artisan reverb:start --port=8080
- php artisan queue:listen
