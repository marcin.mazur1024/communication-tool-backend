<?php

namespace Database\Seeders;

use App\Enums\UserRole;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CommandDeviceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = new User();
        $user->username = 'COMMAND_DEVICE_1';
        $user->password = Hash::make('123qwe');
        $user->role = UserRole::COMMAND_DEVICE;
        $user->save();
    }
}
