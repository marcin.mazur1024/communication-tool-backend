<?php

namespace App\Enums;

enum UserRole: string
{
    case FIELD_DEVICE = 'FIELD_DEVICE';
    case COMMAND_DEVICE = 'COMMAND_DEVICE';

    public static function resolve(string $value): UserRole
    {
        foreach (self::cases() as $enum) {
            if ($value === $enum->value) {
                return $enum;
            }
        }
        return UserRole::FIELD_DEVICE;
    }
}
