<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use stdClass;

class Message extends Model
{
    use HasFactory;

    public static function getMessagesByChannel(int $channelId): mixed
    {
        return Message::where('messages.channel_id', $channelId)->join('users', 'users.id', '=', 'messages.user_id')->select([
            'messages.id as id',
            'messages.user_id as userId',
            'messages.channel_id as channelId',
            'users.username as username',
            'messages.content as content',
            'messages.created_at as createdAt',
        ])->get();
    }

    public static function getMessageById(int $id): stdClass
    {
        $message = Message::where('messages.id', $id)->join('users', 'users.id', '=', 'messages.user_id')->select([
            'messages.id as id',
            'messages.user_id as userId',
            'messages.channel_id as channelId',
            'users.username as username',
            'messages.content as content',
            'messages.created_at as createdAt',
        ])->first();

        if (!isset($message)) return null;

        $out = new stdClass;
        $out->id = $message->id;
        $out->userId = $message->userId;
        $out->channelId = $message->channelId;
        $out->username = $message->username;
        $out->content = $message->content;
        $out->createdAt = $message->createdAt;

        return $out;
    }

    public static function createNewMessage(int $channelId, int $userId, string $content): int
    {
        $msg = new Message();
        $msg->content = $content;
        $msg->user_id = $userId;
        $msg->channel_id = $channelId;
        $msg->save();
        return $msg->id;
    }
}
