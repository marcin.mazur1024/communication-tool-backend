<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Schema(
 *     schema="MessageResource",
 *     title="Message Resource",
 *     description="Represents a message resource",
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         description="ID of the message"
 *     ),
 *     @OA\Property(
 *         property="userId",
 *         type="integer",
 *         description="ID of the user who sent the message"
 *     ),
 *     @OA\Property(
 *         property="channelId",
 *         type="integer",
 *         description="ID of the channel where the message was posted"
 *     ),
 *     @OA\Property(
 *         property="username",
 *         type="string",
 *         description="Username of the user who sent the message"
 *     ),
 *     @OA\Property(
 *         property="content",
 *         type="string",
 *         description="Content of the message"
 *     ),
 *     @OA\Property(
 *         property="createdAt",
 *         type="string",
 *         format="date-time",
 *         description="Date and time when the message was created"
 *     )
 * )
 */
class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $resource = [];
        $resource['id'] = $this->id;
        $resource['userId'] = $this->userId;
        $resource['channelId'] = $this->channelId;
        $resource['username'] = $this->username ? $this->username : Auth::user()->username;
        $resource['content'] = $this->content;
        $resource['createdAt'] = $this->createdAt;
        return $resource;
    }
}
