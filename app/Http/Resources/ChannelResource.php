<?php

namespace App\Http\Resources;

use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *     schema="ChannelResource",
 *     title="Channel Resource",
 *     description="Represents a channel resource",
 *     @OA\Property(
 *         property="userId",
 *         type="integer",
 *         description="ID of the user associated with the channel"
 *     ),
 *     @OA\Property(
 *         property="channelId",
 *         type="integer",
 *         description="ID of the channel"
 *     ),
 *     @OA\Property(
 *         property="username",
 *         type="string",
 *         description="Username of the user associated with the channel"
 *     ),
 *     @OA\Property(
 *         property="content",
 *         type="string",
 *         description="Content of the last message in the channel"
 *     ),
 *     @OA\Property(
 *         property="createdAt",
 *         type="string",
 *         format="date-time",
 *         description="Date and time when the last message in the channel was created"
 *     )
 * )
 */
class ChannelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $message = Message::where('channel_id', $this->user_id)->orderByDesc('id')->first();
        $resource = [];
        $resource['userId'] = $this->user_id;
        $resource['channelId'] = $this->user_id;
        $resource['username'] = $this->username;
        $resource['content'] = $message ? $message->content : null;
        $resource['createdAt'] = $message ? $message->created_at : null;
        return $resource;
    }
}
