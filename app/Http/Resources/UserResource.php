<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *      schema="UserResource",
 *      type="object",
 *      @OA\Property(
 *          property="id",
 *          type="integer",
 *          format="int64",
 *          description="User ID"
 *      ),
 *      @OA\Property(
 *          property="role",
 *          type="string",
 *          description="User role"
 *      ),
 *      @OA\Property(
 *          property="username",
 *          type="string",
 *          description="Username"
 *      )
 * )
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $resource = [];
        $resource['id'] = $this->id;
        $resource['role'] = $this->role;
        $resource['username'] = $this->username;
        return $resource;
    }
}
