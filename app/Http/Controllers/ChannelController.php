<?php

namespace App\Http\Controllers;

use App\Enums\HttpError;
use App\Enums\UserRole;
use App\Http\Resources\ChannelResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ChannelController
{
    /**
     * @OA\Get(
     *     path="/api/channels",
     *     summary="Fetch channels",
     *     description="Fetches channels for the command device",
     *     tags={"Channels"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="channels",
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/ChannelResource")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="error",
     *                 type="string",
     *                 description="Error message"
     *             )
     *         )
     *     )
     * )
     */
    public function fetch(Request $r)
    {
        if (UserRole::resolve(Auth::user()->role) !== UserRole::COMMAND_DEVICE)
            return response()->json(['error' => HttpError::FORBIDDEN], 403);

        $users = User::where('role', UserRole::FIELD_DEVICE)->select([
            'users.username as username',
            'users.id as user_id',
        ])->get();

        return response()->json([
            'channels' => ChannelResource::collection($users),
        ], 200);
    }
}
