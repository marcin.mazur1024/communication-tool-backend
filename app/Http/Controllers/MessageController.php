<?php

namespace App\Http\Controllers;

use App\Enums\HttpError;
use App\Enums\UserRole;
use App\Events\NewMessageEvent;
use App\Http\Resources\MessageResource;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController
{

    /**
     * @OA\Get(
     *     path="/api/messages",
     *     summary="Fetch messages",
     *     description="Fetches messages for the specified channel",
     *     tags={"Messages"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="channelId",
     *         in="query",
     *         required=true,
     *         description="ID of the channel to fetch messages from",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="messages",
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/MessageResource")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="error",
     *                 type="string",
     *                 description="Error message"
     *             )
     *         )
     *     )
     * )
     */
    public function fetch(Request $r)
    {
        $channelId = $r->input('channelId');
        $user = Auth::user();

        if (UserRole::FIELD_DEVICE === UserRole::resolve($user->role) && intval($channelId) !== intval($user->id))
            return response()->json(['error' => HttpError::FORBIDDEN], 403);

        $messages = Message::getMessagesByChannel($channelId);

        return response()->json([
            'messages' => MessageResource::collection($messages),
        ], 200);
    }

    /**
     * @OA\Post(
     *     path="/api/messages",
     *     summary="Create message",
     *     description="Creates a new message for the specified channel",
     *     tags={"Messages"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Message content and channel ID",
     *         @OA\JsonContent(
     *             required={"message", "channelId"},
     *             @OA\Property(
     *                 property="message",
     *                 type="string",
     *                 description="Content of the message"
     *             ),
     *             @OA\Property(
     *                 property="channelId",
     *                 type="integer",
     *                 description="ID of the channel to post the message to"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Message created successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="messages",
     *                 type="array",
     *                 @OA\Items(ref="#/components/schemas/MessageResource")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input data",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="error",
     *                 type="string",
     *                 description="Error message"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="error",
     *                 type="string",
     *                 description="Error message"
     *             )
     *         )
     *     )
     * )
     */
    public function create(Request $r)
    {
        $content = $r->input('message');
        $channelId = $r->input('channelId');

        if (!isset($content) || !isset($channelId))
            return response()->json(['error' => HttpError::INVALID_INPUT_DATA], 400);

        $user = Auth::user();

        if (UserRole::FIELD_DEVICE === UserRole::resolve($user->role) && intval($channelId) !== intval($user->id))
            return response()->json(['error' => HttpError::FORBIDDEN], 403);

        $newMessageId = Message::createNewMessage($channelId, $user->id, $content);
        $message = Message::getMessageById($newMessageId);

        broadcast(new NewMessageEvent($message));

        return response()->json([
            'messages' => [new MessageResource($message)],
        ], 200);
    }
}
