<?php

namespace App\Http\Controllers;

use App\Enums\HttpError;
use App\Enums\UserRole;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * @OA\Info(
 *      title="Communication Tool API",
 *      version="1.0.0",
 *      description="The Communication Tool REST API serves as the backend for a message exchange application, facilitating seamless communication between users. With endpoints for user registration, login, logout, and message creation, this API provides secure access to the application's features. Users can authenticate themselves, send and receive messages, and manage their sessions effortlessly. Built on RESTful principles, the Communication Tool API offers simplicity, reliability, and scalability for a smooth messaging experience.",
 *      contact={
 *          "name": "API Support",
 *          "email": "marcin.mazur1024@gmail.com"
 *      },
 *      license={
 *          "name": "Apache 2.0",
 *          "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
 *      }
 * )
 */
class AuthController
{

    /**
     * @OA\Post(
     *     path="/api/users",
     *     summary="Create user",
     *     description="Creates a new user with the provided username and password",
     *     tags={"Users"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="User credentials",
     *         @OA\JsonContent(
     *             required={"username", "password"},
     *             @OA\Property(
     *                 property="username",
     *                 type="string",
     *                 description="Username of the new user"
     *             ),
     *             @OA\Property(
     *                 property="password",
     *                 type="string",
     *                 description="Password of the new user"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User created successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="token",
     *                 type="string",
     *                 description="Access token for the newly created user"
     *             ),
     *             @OA\Property(
     *                 property="user",
     *                 ref="#/components/schemas/UserResource",
     *                 description="User information"
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input data or username not unique",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="error",
     *                 type="string",
     *                 description="Error message"
     *             )
     *         )
     *     )
     * )
     */
    public function create(Request $r)
    {
        $username = $r->input('username');
        $password = $r->input('password');

        if (!isset($username) || !isset($password))
            return response()->json(['error' => HttpError::INVALID_INPUT_DATA], 400);

        if (User::where('username', $username)->exists())
            return response()->json(['error' => HttpError::USERNAME_NOT_UNIQUE], 400);

        $user = new User();
        $user->username = $username;
        $user->password = Hash::make($password);
        $user->role = UserRole::FIELD_DEVICE;
        $user->save();

        return $this->login($r);
    }

    /**
     * @OA\Post(
     *     path="/api/login",
     *     summary="User login",
     *     description="Authenticates a user with the provided username and password",
     *     tags={"Authentication"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="User credentials",
     *         @OA\JsonContent(
     *             required={"username", "password"},
     *             @OA\Property(
     *                 property="username",
     *                 type="string",
     *                 description="Username of the user"
     *             ),
     *             @OA\Property(
     *                 property="password",
     *                 type="string",
     *                 description="Password of the user"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User authenticated successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="user",
     *                 ref="#/components/schemas/UserResource",
     *                 description="User information"
     *             ),
     *             @OA\Property(
     *                 property="token",
     *                 type="string",
     *                 description="Access token for the authenticated user"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid credentials",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="error",
     *                 type="string",
     *                 description="Error message"
     *             )
     *         )
     *     )
     * )
     */
    public function login(Request $r)
    {
        $username = $r->input('username');
        $password = $r->input('password');

        Auth::attempt(['username' => $username, 'password' => $password]);
        if (!Auth::check())
            return response()->json(['error' => HttpError::INVALID_CREDENTIALS], 400);

        $user = Auth::user();

        return response()->json([
            'user' => new UserResource($user),
            'token' => $user->createToken(env('APP_NAME'))->accessToken,
        ], 200);
    }

    /**
     * @OA\Post(
     *     path="/api/logout",
     *     summary="User logout",
     *     description="Logs out the authenticated user and invalidates all access tokens",
     *     tags={"Authentication"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="User logged out successfully"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     )
     * )
     */
    public function logout(Request $r)
    {
        $user = Auth::user();
        $user->tokens()->delete();
        return response()->json([], 200);
    }
}
