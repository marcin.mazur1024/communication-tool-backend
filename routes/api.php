<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\MessageController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'user'], function () {
    Route::post('', [AuthController::class, 'create']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:api');
});

Route::middleware(['auth:api'])->group(function () {
    Route::group(['prefix' => 'message'], function () {
        Route::post('', [MessageController::class, 'create']);
        Route::get('', [MessageController::class, 'fetch']);
    });

    Route::group(['prefix' => 'channel'], function () {
        Route::get('', [ChannelController::class, 'fetch']);
    });
});
