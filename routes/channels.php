<?php

use App\Enums\UserRole;
use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('channel.{id}', function ($user, $id) {
    return UserRole::COMMAND_DEVICE === UserRole::resolve($user->role) || intval($user->id) === intval($id);
});
